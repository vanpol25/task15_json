package com.polahniuk.view;


import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.polahniuk.model.Printable;
import com.polahniuk.model.candy.Candy;
import com.polahniuk.model.gson.GsonParser;
import com.polahniuk.model.jackson.JacksonParser;
import com.polahniuk.model.validator.Validator;
import org.apache.logging.log4j.*;

import java.io.File;
import java.io.IOException;
import java.util.*;

/**
 * Main view of project. Show menu in console.
 */
public class Menu {

    private Logger log = LogManager.getLogger(Menu.class);
    private Scanner sc = new Scanner(System.in);
    private Map<String, String> menu;
    private Map<String, Printable> methods;

    /**
     * Creates methods in constructor due to interface{@link Printable}.
     */
    public Menu() {
        setMenu();
        methods = new LinkedHashMap<>();
        methods.put("1", this::validator);
        methods.put("2", this::gsonParser);
        methods.put("3", this::jacksonParser);
        show();
    }


    /**
     * Creating menu.
     */
    private void setMenu() {
        menu = new LinkedHashMap<>();
        menu.put("1", "1 - Validator");
        menu.put("2", "2 - Task 2");
        menu.put("3", "3 - Task 3");
        menu.put("4", "4 - Task 4");
        menu.put("5", "5 - Task 5");
        menu.put("6", "6 - Task 6");
        menu.put("7", "7 - Task 7");
        menu.put("8", "8 - Task 2.1");
        menu.put("9", "9 - Task 2.2");
        menu.put("10", "10 - Task 2.3");
        menu.put("Q", "Q - Exit");
    }

    private void validator() {
        File json = new File("C:\\Users\\Van_PC\\git\\task15_json\\json\\src\\main\\resources\\json\\candies.json");
        File schema = new File("C:\\Users\\Van_PC\\git\\task15_json\\json\\src\\main\\resources\\json\\schema.json");

        try {
            boolean isValid = Validator.validate(schema, json);
            System.out.println(isValid);
        } catch (ProcessingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void gsonParser() {
        File json = new File("C:\\Users\\Van_PC\\git\\task15_json\\json\\src\\main\\resources\\json\\candies.json");
        GsonParser g = new GsonParser();
        List<Candy> list = g.getCandiesFromJson(json);
        System.out.println(list);
    }

    private void jacksonParser() {
        File json = new File("C:\\Users\\Van_PC\\git\\task15_json\\json\\src\\main\\resources\\json\\candies.json");
        JacksonParser j = new JacksonParser();
        List<Candy> list = j.getCandiesFromJson(json);
        System.out.println(list);
    }

    /**
     * Show menu in console.
     */
    private void getMenu() {
        System.out.println("\nMENU:");
        for (String key : menu.keySet()) {
            System.out.println(menu.get(key));
        }
    }

    /**
     * Main method to work with user in console.
     */
    private void show() {
        String keyMenu;
        do {
            getMenu();
            System.out.println("Please, select menu point.");
            keyMenu = sc.nextLine().toUpperCase();
            try {
                methods.get(keyMenu).print();
            } catch (Exception e) {
                log.info(e);
            }
        } while (!keyMenu.equals("Q"));
    }

}