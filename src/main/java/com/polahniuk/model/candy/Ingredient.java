package com.polahniuk.model.candy;

import lombok.Data;

import java.util.List;

@Data
public class Ingredient {

    private List<String> ingredients;

}
