package com.polahniuk.model.candy;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * Class candy.
 */
@Data
public class Candy {

    private String name;
    private int energy;
    private Type type;
    private Ingredient ingredient;
    private Value value;
    private String production;

    public Candy() {
    }

    @Override
    public String toString() {
        return "\nCandy{" +
                "name='" + name + '\'' +
                ", energy=" + energy +
                ", stuffing=" + type +
                ", ingredient=" + ingredient +
                ", value=" + value +
                ", production='" + production + '\'' +
                '}';
    }
}