package com.polahniuk.model.candy;

import lombok.Data;

/**
 * Class has information about stuffing in candy.
 */
@Data
public class Type {

    private boolean stuffing;
    private String text;

    @Override
    public String toString() {
        return "Type{" +
                "stuffing='" + stuffing + '\'' +
                ", include=" + text+
                '}';
    }
}