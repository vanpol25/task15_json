package com.polahniuk.model.candy;

import lombok.Data;

/**
 * Class value.
 */
@Data
public class Value {

    private double proteins;
    private double fats;
    private double carbohydrates;

    @Override
    public String toString() {
        return "Value{" +
                "Proteins=" + proteins +
                ", Fats=" + fats +
                ", Carbohydrates=" + carbohydrates +
                '}';
    }
}