package com.polahniuk.model.candy;

import lombok.Data;

import java.util.List;

/**
 * Class sweet.
 */
@Data
public class Sweet {

    private List<Candy> candies;

    @Override
    public String toString() {
        return candies.toString();
    }
}
