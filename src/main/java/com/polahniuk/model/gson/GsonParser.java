package com.polahniuk.model.gson;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.polahniuk.model.candy.Candy;
import com.polahniuk.model.candy.Sweet;
import com.polahniuk.model.jackson.JacksonParser;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class GsonParser {

    private Logger log = LogManager.getLogger(GsonParser.class);
    private Gson gson;

    public GsonParser() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gson = gsonBuilder.create();
    }

    public List<Candy> getCandiesFromJson(File file) {
        Candy[] candies = null;
        try {
            candies = gson.fromJson(new FileReader(file), Candy[].class);
        } catch (FileNotFoundException e) {
            log.info(e);
        }
        return Arrays.asList(candies);
    }
}
