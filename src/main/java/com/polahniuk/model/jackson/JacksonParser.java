package com.polahniuk.model.jackson;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.polahniuk.model.candy.Candy;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class JacksonParser {

    private Logger log = LogManager.getLogger(JacksonParser.class);
    private ObjectMapper objectMapper;

    public JacksonParser() {
        this.objectMapper = new ObjectMapper();
    }

    public List<Candy> getCandiesFromJson(File jsonFile) {
        Candy[] candies = null;
        try {
            candies = objectMapper.readValue(jsonFile, Candy[].class);
        } catch (JsonParseException | JsonMappingException e) {
            log.info(e);
        } catch (IOException e) {
            log.info(e);
        }
        return Arrays.asList(candies);
    }
}
